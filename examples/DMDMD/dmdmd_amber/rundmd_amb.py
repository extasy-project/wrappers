from wrappers import amber, lsdmap, selection, reweighting
from wrappers.tools import pack
import dask.bag as db

start = 'penta.crd'
topol = 'penta.top'
pdbfile = 'penta.pdb'
mdin = 'md.in'
atom_selection = "'not element == H'"
nreps = 50
ncycles = 5

amber.DEFAULTS['-i'] = mdin
amber.DEFAULTS['-p'] = topol
amber.DEFAULTS['-r'] = '{}.rst7'

lsdmap.DEFAULTS['-f'] = 'lsdmap.ini'
lsdmap.DEFAULTS['-s'] = atom_selection

reweighting.DEFAULTS['--max_alive_neighbors'] = 10
reweighting.DEFAULTS['--max_dead_neighbors'] = 1

startfiles = [start,] * nreps
wfile = None

for cycle in range(ncycles):
    minps = []
    for i in range(nreps):
        minp = amber.new_inputs(defdir='results')
        minp['-c'] = startfiles[i]
        minps.append(minp)

    b = db.from_sequence(minps).map(amber.run)
    print 'Starting AMBER runs...'
    mouts = b.compute()

    crdfiles = [mout['-r'] for mout in mouts]
    trjfile = pack.pack(crdfiles)

    print 'Running LSDMap...'
    linp = lsdmap.new_inputs(defdir='results')
    linp['-c'] = crdfiles
    linp['-t'] = pdbfile
    if wfile is not None:
        linp['-w'] = wfile

    lout = lsdmap.run(linp)

    print 'Running selection...'
    sinp = selection.new_inputs(defdir='results')
    sinp['-s'] = lout['-ev']
    sinp[1] = nreps

    sout = selection.run(sinp)

    print 'Running reweighting...'
    rinp = reweighting.new_inputs(defdir='results')
    rinp['-c'] = trjfile
    rinp['-n'] = lout['-n']
    rinp['-s'] = sout['-o']

    rout = reweighting.run(rinp)

    startfiles = pack.unpack(rout['-o'])
    nreps = len(startfiles)
    wfile = rout['-w']

A demonstration of how CoCo can be used in an enhanced sampling workflow

The python script shows how pyCoCo, and AMBER, jobs can be run through a
simple API.

NOTE: You need to have Amber (or Ambertools) installed, and have the command
"sander" in your PATH.

The workflow is as follows:

1. Starting with a single structure (of the alanine pentapeptide), eight short 
replicate MD simulations are run, using AMBER.
2. The eight trajectory files are analysed using CoCo and eight new structures,
corresponding to unsampled regions in the conformational distribution, are 
generated.
3. Further MD is run on each of these
4. Back to step 2, including both newly-generated and previous cycles of 
trajectory files in the analysis.

To run the workflow, type:

python penta_coco.py

The analysis of final ensemble of trajectory files is in the log file 
CoCo_final.log.

To assess how well application of CoCo enhances the sampling of conformational 
space, the script "penta_nococo.py" will run an equivalent workflow, but 
omitting the CoCo step.

Comparison of the final CoCo log files (CoCo_final.log, noCoCo_final.log) will
reveal the greater variance in the CoCo-enhanced ensemble.

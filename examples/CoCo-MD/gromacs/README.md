This example demonstrated a Gromacs-CoCo enhanced sampling workflow. The test system is the alainine pentapeptide.

To run type:

python penta_coco.py

Note: you may wish to:

module load gromacs
setenv GMX_MAXBACKUP -1

first.

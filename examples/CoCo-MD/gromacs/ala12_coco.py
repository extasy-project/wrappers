from wrappers import grompp, mdrun, pycoco
import dask.bag as db
import os

start = 'helix.gro'
topol = 'topol.top'
atom_selection = "'all'"
nreps = 8
ncycles = 10

# create a list that will hold all the trajectory file names
mdtrajectories = []

# put intermediate files in a subdirectory to keep things tidy:
defdir = 'cocofiles'

# define some defaults for the MD and CoCo analysis

pycoco.DEFAULTS['-o'] = '{}.gro'
pycoco.DEFAULTS['-f'] = 'gro'
pycoco.DEFAULTS['-t'] = 'helix.gro'
pycoco.DEFAULTS['-n'] = nreps
pycoco.DEFAULTS['--dims'] = 3
pycoco.DEFAULTS['--grid'] = 30

grompp.DEFAULTS['-p'] = topol

wfile = None

for cycle in range(ncycles):
    if cycle == 0:
#
# Cycle 0: We begin with a single coordinate and topology file. 
# Create a "dummy" coco results dictionary with copies of the initial
# coordinates under the '-o' key:
#
        print 'setting up...'
        cocores = {}
        cocores['-o'] = [start,] * nreps
    else:        
        print 'Cycle {}: running CoCo...'.format(cycle-1)
        
        # Set pyCoCo parameters:
        coinp = pycoco.new_inputs(defdir=defdir)
        coinp['-i'] = mdtrajectories

        # Run job:
        cocores = pycoco.run(coinp)

#
# Now run the three MD jobs - restrained then unrestrained equilibration,
# then production MD..
#
    ginps = []
    for i in range(nreps):
        ginp = grompp.new_inputs(defdir=defdir)
        ginp['-c'] = start
        ginp['-r'] = cocores['-o'][i]
        ginp['-f'] = 'grompp-em1.mdp'
        
        ginps.append(ginp)

    b = db.from_sequence(ginps).map(grompp.run)
    print 'Starting grompp runs...'
    gouts = b.compute()

    minps = []
    for i in range(nreps):
        minp = mdrun.new_inputs(defdir=defdir)
        minp['-s'] = gouts[i]['-o']
        minps.append(minp)

    b = db.from_sequence(minps).map(mdrun.run)
    print 'Starting MD jobs 1...'
    mouts = b.compute()

    ginps = []
    for i in range(nreps):
        ginp = grompp.new_inputs(defdir=defdir)
        ginp['-c'] = mouts[i]['-c']
        ginp['-f'] = 'grompp-em2.mdp'
        
        ginps.append(ginp)

    b = db.from_sequence(ginps).map(grompp.run)
    print 'Starting grompp runs...'
    gouts = b.compute()

    minps = []
    for i in range(nreps):
        minp = mdrun.new_inputs(defdir=defdir)
        minp['-s'] = gouts[i]['-o']
        minps.append(minp)

    b = db.from_sequence(minps).map(mdrun.run)
    print 'Starting MD jobs 2...'
    mouts = b.compute()

    ginps = []
    for i in range(nreps):
        ginp = grompp.new_inputs(defdir=defdir)
        ginp['-c'] = mouts[i]['-c']
        ginp['-f'] = 'grompp-verlet.mdp'
        
        ginps.append(ginp)

    b = db.from_sequence(ginps).map(grompp.run)
    print 'Starting grompp runs...'
    gouts = b.compute()

    minps = []
    for i in range(nreps):
        minp = mdrun.new_inputs(defdir=defdir)
        minp['-s'] = gouts[i]['-o']
        minps.append(minp)

    b = db.from_sequence(minps).map(mdrun.run)
    print 'Starting MD jobs 3...'
    mouts = b.compute()

    mdtrajectories = mdtrajectories + [mout['-x'] for mout in mouts]

#
# final CoCo run...
#
print 'Cycle {}: running CoCo...'.format(cycle)
        
coinp = pycoco.new_inputs(defdir=defdir)
coinp['-i'] = mdtrajectories

cocores = pycoco.run(coinp)

print 'final results in CoCo_final.log'
os.rename(cocores['-l'], 'CoCo_final.log')
os.system('rm '+defdir+'/tmp*')

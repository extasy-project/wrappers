A demonstration of how to write parallelised CoCo workflows!

A. Using Dask (such as in para_coco.py).

Replicate min/md jobs in the para_coco.py script are run in
parallel, and (to make the example a little more impressive) the number of
replicates is 80 (the user can modify this number as she wishes).

B. Using MPI (such as in mpi_coco.py) 
The script mpi_coco.py shows how you can use the mpipool library (github.com/adrn/mpipool) as an alternative to Dask.
